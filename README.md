# Dotfiles
### IMPORTANT
This repository is a part of stack which I use to bootstrap, operate, store and version control all my work environment at various levels, such as:

1. [Operating system](https://gitlab.com/ChipotleSouthwest/kickstart)
2. [System infill (packages, parameters and so on)](https://gitlab.com/ChipotleSouthwest/ansible)
3. [Dotfiles](https://gitlab.com/ChipotleSouthwest/dotfiles)

Now this whole "project" is just the first version so any advice, ideas, suggestions or fixes are welcome. Feel free to open an issue!

This and all other repositories will be expanding as I use it.

You won't find any demonstration gifs, screenshots or videos here because I believe all of these things should be used, not watched. Also I don't think I am such good lector so I don't repeat any documentation here - you can google it by yourself.

### My  dotfiles
I use many software and tools to increase my work performance and there you can find some configuration and functions for all that stuff. When I understand how kde store and use its configuration files I will add them too. Also I have plans to build and configure my workflow with NeoVim and automate signing in VSCode account to sync all settings.

Whole idea and workflow described by StreakyCobra with [his answer at Hacker News](https://news.ycombinator.com/item?id=11070797).

### WARNING
I prefer to run all my stuff when system launched so it can be confusing. You can disable it removing all files from `~/.config/autostart` (btw if you know how to launch all of that minimized - let me know).
